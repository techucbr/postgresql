--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.10
-- Dumped by pg_dump version 9.5.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: techu
--

CREATE TABLE categories (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    description character varying(300)
);


ALTER TABLE categories OWNER TO techu;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: techu
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq OWNER TO techu;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: techu
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: techu
--

CREATE TABLE products (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    description character varying(300),
    file character varying(200) DEFAULT 'gif'::character varying NOT NULL
);


ALTER TABLE products OWNER TO techu;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: techu
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_id_seq OWNER TO techu;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: techu
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: user_accounts; Type: TABLE; Schema: public; Owner: techu
--

CREATE TABLE user_accounts (
    product_id integer NOT NULL,
    user_id integer NOT NULL,
    id integer NOT NULL,
    balance numeric NOT NULL,
    code character varying(20) NOT NULL,
    alias character varying(200)
);


ALTER TABLE user_accounts OWNER TO techu;

--
-- Name: user_products_id_seq; Type: SEQUENCE; Schema: public; Owner: techu
--

CREATE SEQUENCE user_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_products_id_seq OWNER TO techu;

--
-- Name: user_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: techu
--

ALTER SEQUENCE user_products_id_seq OWNED BY user_accounts.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: techu
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(100) NOT NULL,
    name character varying(100) NOT NULL,
    surname character varying(100) NOT NULL,
    password character varying(200) NOT NULL,
    admin boolean DEFAULT false NOT NULL
);


ALTER TABLE users OWNER TO techu;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: techu
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO techu;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: techu
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: techu
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: techu
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: techu
--

ALTER TABLE ONLY user_accounts ALTER COLUMN id SET DEFAULT nextval('user_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: techu
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: techu
--

COPY categories (id, name, description) FROM stdin;
1	Ocio	Categoria de ocio
3	Impuestos	Categoria de impuestos
2	Colegio	Categoria de restaurantes
4	Nómina	Categoría de Nómina
5	Transferencia	Categoría de transferencia
8	Navidades	Categoría de Navidad
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: techu
--

SELECT pg_catalog.setval('categories_id_seq', 9, true);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: techu
--

COPY products (id, name, description, file) FROM stdin;
1	Cuenta corriente	Es una cuenta corriente al uso	account.png
2	Tarjeta de crédito	Tarjeta de crédito VISA	card.png
14	Plan de pensiones	Plan de pensiones sin riesgo	dott2.png
\.


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: techu
--

SELECT pg_catalog.setval('products_id_seq', 15, true);


--
-- Data for Name: user_accounts; Type: TABLE DATA; Schema: public; Owner: techu
--

COPY user_accounts (product_id, user_id, id, balance, code, alias) FROM stdin;
1	2	15	42.98	22	22
1	1	1	1293.87	10	Cuenta Casa
1	2	5	0.01	21	Cuenta express
2	1	4	1234.56	12	Tarjeta de crédito
1	1	2	98732.87	11	Cuenta Común
3	2	16	908732.87	23	Mi plan de pensiones
\.


--
-- Name: user_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: techu
--

SELECT pg_catalog.setval('user_products_id_seq', 16, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: techu
--

COPY users (id, username, name, surname, password, admin) FROM stdin;
12	dddd@ddd.com	Gustavo	de Básica	sha1$c9946b68$1$69d0cb09965fe26b00801c37e37ac9ff4e4d1495	f
2	ma@a.es	María	González	sha1$802fd6d7$1$a0ec78e4a323bb7c05d6219d48d46d74f9d55a41	f
14	c@c.cc	Sara	Gómez	sha1$ca0ce22c$1$21f094a713c461ccfb132877e9871e758e39c920	f
1	a@a.aa	Carlos	Bueno	sha1$e2b012bd$1$fe74f5c25a10baf7cc2020e5cfba2cee6682a653	t
20	m@j.es	Marco	Jones	sha1$ad2039d5$1$70d9fc34c2d8c990b107cbd81aa5e5a68d3c5094	f
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: techu
--

SELECT pg_catalog.setval('users_id_seq', 20, true);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: techu
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: techu
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: user_products_code_key; Type: CONSTRAINT; Schema: public; Owner: techu
--

ALTER TABLE ONLY user_accounts
    ADD CONSTRAINT user_products_code_key UNIQUE (code);


--
-- Name: user_products_pkey; Type: CONSTRAINT; Schema: public; Owner: techu
--

ALTER TABLE ONLY user_accounts
    ADD CONSTRAINT user_products_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: techu
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: user_products_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: techu
--

ALTER TABLE ONLY user_accounts
    ADD CONSTRAINT user_products_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: user_products_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: techu
--

ALTER TABLE ONLY user_accounts
    ADD CONSTRAINT user_products_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

